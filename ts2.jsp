<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="mod" tagdir="/WEB-INF/tags/terrific/modules" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:theme code="qualityandlegal.fileSizeDeniedMessage" var="sFileSizeDeniedMessage"/>
<spring:theme code="qualityandlegal.fileSizeMessage" var="sFileSizeMessage"/>
<spring:theme code="qualityandlegal.fileTypeDeniedMessage" var="sFileTypeDeniedMessage"/>
<spring:theme code="qualityandlegal.fileTypeMessage" var="sFileTypeMessage"/>
<spring:theme code="qualityandlegal.introduction" var="sIntroduction"/>
<spring:theme code="qualityandlegal.reachScipDocumentation" var="sReachScipDocumentation"/>
<spring:theme code="qualityandlegal.docsDownload" var="sDocsDownload"/>
<spring:theme code="qualityandlegal.bulkDownload" var="sBulkDownload"/>
<spring:theme code="qualityandlegal.excelTemplate" var="sExcelTemplate"/>
<spring:theme code="qualityandlegal.pdfTemplate" var="sPdfTemplate"/>

<div class="mod mod-quality-page-upload">
    <div class="content-banner">
        <h2>${sReachScipDocumentation}</h2>
    </div>
    <h3>${sDocsDownload}</h3>
    ${sIntroduction}

    <form:form action="/environmental-documentation-download/upload-file?${_csrf.parameterName}=${_csrf.token}" modelAttribute="file"
               id="upload-form-text" enctype="multipart/form-data" method="post">
        <div class="content">

            <div class="content__item">
                <form:input id="file" type="file" path="file"
                       data-file-types="xls,xlsx,csv,txt"
                       data-max-file-size="1048576"
                       data-file-size-denied-message="${sFileSizeDeniedMessage}"
                       data-file-size-message="${sFileSizeMessage}"
                       data-file-type-denied-message="${sFileTypeDeniedMessage}"
                       data-file-type-message="${sFileTypeMessage}"
                />

                <div class="advanced-upload">

                    <a href="#" class="upload-file boxy">
                        <div class="upload-file__item">

                            <div class="browser-other">
										<span class="icon">
											<i class="fas fa-cloud-upload-alt" aria-hidden="true"></i>
										</span>
                                <span class="text">
											<spring:theme code="bomdataimport.uploaddescription"/>
										</span>

                            </div>

                            <div class="browser-ie mat-button mat-button__solid--action-blue" href="/welcome">
                                <i class="fas fa-cloud-upload-alt" aria-hidden="true"></i>
                                <spring:theme code="bomdataimport.uploaddescription"/>
                            </div>

                            <span class="filename">sampledata.xlsx</span>

                        </div>

                    </a>
                </div>
                <div class="errors">
                    <p></p>
                </div>
            </div>
        </div>
        <div class="btn-holder">
            <button type="submit" class="mat-button mat-button__solid--action-green btn-continue"><spring:theme
                    code="bomdataimport.continue"/><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
        </div>
    </form:form>
    <c:url value="/environmental-documentation-download/downloadXlsReport" var="excelDownloadUrl" />
    <a href="${excelDownloadUrl}">${sExcelTemplate}</a><br>
    <a href="#">${sPdfTemplate}</a><br>
    <button class="bulk-download" disabled>${sBulkDownload}</button>
</div>

