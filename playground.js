let counter = 30;
if (counter === 30) {
  let counter = 31;
  console.log(counter); // 31
}
console.log(counter); // 30 (because the variable in if block won't exist here)

let blockvar =  10

function firstFunction () {
  let blockvar =  11

  function secondFunction(){
    console.log(blockvar); 
   }
return secondFunction
}
var closureFunction = firstFunction()

closureFunction()


let uri = "employeeDetails?name=john&occupation=manager";
let encoded_uri = encodeURI(uri);
let decoded_uri = decodeURI(encoded_uri);

console.log(decoded_uri);

/* Array of objects*/

let cars = [
  {
    "color": "purple",
    "type": "minivan",
    "registration": new Date('2017-01-03'),
    "capacity": 7
  },
  {
    "color": "red",
    "type": "station wagon",
    "registration": new Date('2018-03-03'),
    "capacity": 5
  },
  {
    "color": "blue",
    "type": "sports",
    "registration": new Date('2019-02-03'),
    "capacity": 2
  },
]

console.log(cars);
let callBackFilter = x => x.color !== 'red';
let newCars = cars.filter(callBackFilter).map(x => x.color)

console.log(newCars);


/* Objects */

const employees = {
  engineers: {
      0: {
          id: 1,
          name: "John Doe",
          occupation: "Back-end Engineer"
      },
      1: {
          id: 2,
          name: "Jane Doe",
          occupation: "Front-end Engineer"
      },
  }
};
const {
  engineers: {
      1: {
          id,
          name,
          occupation,
      },
  },
} = employees;
console.log(name);
/* for (const employees of object) {
  console.log(employees.engineers[1]);
} */
console.log(Object.entries(employees));

  
for (const key in employees) {
  const el = employees[key];
  for (const key in el) {
    const el2 = el[key];
    const obj = Object.assign({}, el2);

    console.log(obj);
  }
  /* if (Object.hasOwnProperty.call(object, key)) {
    const element = object[key];
    
  } */
}

/* 
Merging objects
 */

const profile = {
  name: 'John Doe',
  age: 25
};

const job = {
  profession: 'IT Engineer'
};

const newObj = Object.assign( profile, job);
console.log(newObj);

function smallestCommons(args) {
  let ary = []
 for (let i = 0; i < args.length; i++) {
   const e = args[i];
   ary.push(e)

 }
  return ary
}


console.log(smallestCommons([1, 5]));


 function find_max(nums) {
   let max_num = Number.NEGATIVE_INFINITY; // smaller than all other numbers
   for (let num of nums) {
   if (num > max_num) {
    max_num = num
   }
   }
   return max_num;
  }
  console.log(find_max([1,3,22,4,6]));