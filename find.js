var ITEMS_WITH_WARRANTY = [
    {
      sku: 16901348,
      warranty: 1,
    },
    {
      sku: 17605827,
      warranty: 'lifetime',
    },
    {
      sku: 17605421,
      warranty: 'lifetime',
    },
    {
      sku: 30122459,
      warranty: 3,
    },
    {
      sku: 30047575,
      warranty: 'lifetime',
    },
    {
      sku: 30122458,
      warranty: 3,
    },
    {
      sku: 17605843,
      warranty: 'lifetime',
    },
    {
      sku: 17605835,
      warranty: 'lifetime',
    },
    {
      sku: 17605447,
      warranty: 'lifetime',
    },
    {
      sku: 11030315,
      warranty: 2,
    },
    {
      sku: 30167674,
      warranty: 3,
    },
    {
      sku: 17641233,
      warranty: 2,
    },
    {
      sku: 11071704,
      warranty: 5,
    },
    {
      sku: 17608755,
      warranty: 'lifetime',
    },
    {
      sku: 17641012,
      warranty: 2,
    },
    {
      sku: 17609213,
      warranty: 'lifetime',
    },
    {
      sku: 17605408,
      warranty: 'lifetime',
    },
    {
      sku: 30047586,
      warranty: 3,
    },
    {
      sku: 30131738,
      warranty: 'lifetime',
    },
    {
      sku: 17646466,
      warranty: 1,
    },
    {
      sku: 30179252,
      warranty: 2,
    },
    {
      sku: 30108747,
      warranty: 3,
    },
    {
      sku: 30008754,
      warranty: 1,
    },
    {
      sku: 17609184,
      warranty: 3,
    },
    {
      sku: 11041944,
      warranty: 3,
    },
    {
      sku: 30037363,
      warranty: 3,
    },
    {
      sku: 30178422,
      warranty: 5,
    },
    {
      sku: 30011644,
      warranty: 3,
    },
    {
      sku: 17674286,
      warranty: 5,
    },
    {
      sku: 17600190,
      warranty: 'lifetime',
    },
    {
      sku: 30131808,
      warranty: 'lifetime',
    },
    {
      sku: 30119651,
      warranty: 'lifetime',
    },
    {
      sku: 30122508,
      warranty: 3,
    },
    {
      sku: 17671119,
      warranty: 3,
    },
    {
      sku: 30160863,
      warranty: 1,
    },
    {
      sku: 17629020,
      warranty: 1,
    },
    {
      sku: 17605821,
      warranty: 'lifetime',
    },
    {
      sku: 30122506,
      warranty: 3,
    },
    {
      sku: 16985256,
      warranty: 2,
    },
    {
      sku: 17646458,
      warranty: 1,
    },
    {
      sku: 16985121,
      warranty: 3,
    },
    {
      sku: 30092156,
      warranty: 2,
    },
    {
      sku: 17609210,
      warranty: 'lifetime',
    },
    {
      sku: 30092155,
      warranty: 2,
    },
    {
      sku: 17649395,
      warranty: 3,
    },
    {
      sku: 17640711,
      warranty: 3,
    },
    {
      sku: 17651755,
      warranty: 1,
    },
    {
      sku: 17608765,
      warranty: 'lifetime',
    },
    {
      sku: 17640709,
      warranty: 3,
    },
    {
      sku: 17609258,
      warranty: 'lifetime',
    },
    {
      sku: 30131814,
      warranty: 'lifetime',
    },
    {
      sku: 16985164,
      warranty: 3,
    },
    {
      sku: 11070977,
      warranty: 2,
    },
    {
      sku: 30118738,
      warranty: 3,
    },
    {
      sku: 30047587,
      warranty: 3,
    },
    {
      sku: 30136532,
      warranty: 1,
    },
    {
      sku: 30101671,
      warranty: 3,
    },
    {
      sku: 16971281,
      warranty: 1,
    },
    {
      sku: 30136531,
      warranty: 1,
    },
    {
      sku: 30157058,
      warranty: 3,
    },
    {
      sku: 17651804,
      warranty: 1,
    },
    {
      sku: 17610306,
      warranty: 1,
    },
    {
      sku: 30135121,
      warranty: 2,
    },
    {
      sku: 30129274,
      warranty: 3,
    },
    {
      sku: 17651805,
      warranty: 1,
    },
    {
      sku: 17651763,
      warranty: 1,
    },
    {
      sku: 30060453,
      warranty: 3,
    },
    {
      sku: 30088363,
      warranty: 2,
    },
    {
      sku: 30111506,
      warranty: 5,
    },
    {
      sku: 30020270,
      warranty: 2,
    },
    {
      sku: 30047580,
      warranty: 3,
    },
    {
      sku: 30216592,
      warranty: 1,
    },
    {
      sku: 30157057,
      warranty: 3,
    },
    {
      sku: 30035603,
      warranty: 'lifetime',
    },
    {
      sku: 30082459,
      warranty: 3,
    },
    {
      sku: 30216593,
      warranty: 1,
    },
    {
      sku: 30135119,
      warranty: 2,
    },
    {
      sku: 30089788,
      warranty: 3,
    },
    {
      sku: 30008858,
      warranty: 3,
    },
    {
      sku: 30035464,
      warranty: 3,
    },
    {
      sku: 17651951,
      warranty: 1,
    },
    {
      sku: 16984397,
      warranty: 3,
    },
    {
      sku: 30009936,
      warranty: 3,
    },
    {
      sku: 30047585,
      warranty: 3,
    },
    {
      sku: 11084986,
      warranty: 3,
    },
    {
      sku: 16984195,
      warranty: 1,
    },
    {
      sku: 17608790,
      warranty: 3,
    },
    {
      sku: 11070325,
      warranty: 1,
    },
    {
      sku: 30167895,
      warranty: 2,
    },
    {
      sku: 17651952,
      warranty: 1,
    },
    {
      sku: 30116479,
      warranty: 2,
    },
    {
      sku: 30047582,
      warranty: 3,
    },
    {
      sku: 11048971,
      warranty: 1,
    },
    {
      sku: 30108748,
      warranty: 3,
    },
    {
      sku: 17631252,
      warranty: 3,
    },
    {
      sku: 30060476,
      warranty: 3,
    },
    {
      sku: 17636291,
      warranty: 3,
    },
    {
      sku: 30092473,
      warranty: 2,
    },
    {
      sku: 30085386,
      warranty: 5,
    },
    {
      sku: 30135120,
      warranty: 2,
    },
    {
      sku: 30089789,
      warranty: 3,
    },
    {
      sku: 30176860,
      warranty: 5,
    },
    {
      sku: 11085096,
      warranty: 3,
    },
    {
      sku: 30103334,
      warranty: 1,
    },
    {
      sku: 16985841,
      warranty: 3,
    },
    {
      sku: 30074585,
      warranty: 1,
    },
    {
      sku: 30159891,
      warranty: 3,
    },
    {
      sku: 11075336,
      warranty: 5,
    },
    {
      sku: 17636290,
      warranty: 3,
    },
    {
      sku: 30020271,
      warranty: 2,
    },
    {
      sku: 11085486,
      warranty: 2,
    },
    {
      sku: 30045658,
      warranty: 2,
    },
    {
      sku: 30176862,
      warranty: 5,
    },
    {
      sku: 30045653,
      warranty: 2,
    },
    {
      sku: 11085487,
      warranty: 2,
    },
    {
      sku: 30147193,
      warranty: 2,
    },
    {
      sku: 30108360,
      warranty: 3,
    },
    {
      sku: 11085499,
      warranty: 2,
    },
    {
      sku: 17636292,
      warranty: 3,
    },
    {
      sku: 30035626,
      warranty: 3,
    },
    {
      sku: 30167896,
      warranty: 2,
    },
    {
      sku: 30158784,
      warranty: 2,
    },
    {
      sku: 11068531,
      warranty: 2,
    },
    {
      sku: 30152641,
      warranty: 3,
    },
    {
      sku: 30185783,
      warranty: 3,
    },
    {
      sku: 11085443,
      warranty: 2,
    },
    {
      sku: 30075605,
      warranty: 1,
    },
    {
      sku: 11070328,
      warranty: 1,
    },
    {
      sku: 30200926,
      warranty: 2,
    },
    {
      sku: 30164161,
      warranty: 3,
    },
    {
      sku: 30152655,
      warranty: 3,
    },
    {
      sku: 30178519,
      warranty: 1,
    },
    {
      sku: 30177301,
      warranty: 2,
    },
    {
      sku: 30200923,
      warranty: 2,
    },
    {
      sku: 30145097,
      warranty: 5,
    },
    {
      sku: 17661690,
      warranty: 1,
    },
    {
      sku: 30159890,
      warranty: 3,
    },
    {
      sku: 30108361,
      warranty: 3,
    },
    {
      sku: 11085448,
      warranty: 2,
    },
    {
      sku: 30186127,
      warranty: 3,
    },
    {
      sku: 11068529,
      warranty: 2,
    },
    {
      sku: 30148542,
      warranty: 1,
    },
    {
      sku: 30162750,
      warranty: 1,
    },
    {
      sku: 30104375,
      warranty: 3,
    },
    {
      sku: 30100570,
      warranty: 3,
    },
    {
      sku: 11085506,
      warranty: 2,
    },
    {
      sku: 30159888,
      warranty: 3,
    },
    {
      sku: 11085455,
      warranty: 2,
    },
    {
      sku: 30145107,
      warranty: 5,
    },
    {
      sku: 17636320,
      warranty: 5,
    },
    {
      sku: 11070326,
      warranty: 1,
    },
    {
      sku: 30172028,
      warranty: 3,
    },
  ];
  function waitFor(condition, timeout) {
    var stopTime = typeof timeout === 'number' ? timeout : 7000;
    var stop = false;
    window.setTimeout(function() {
      stop = true;
    }, stopTime);
    return new Promise(function(resolve, reject) {
      (function _innerWaitFor() {
        var value = condition();

        if (stop) {
          reject();
        } else if (value) {
          resolve(value);
        } else {
          window.setTimeout(_innerWaitFor, 50);
        }
      })();
    });
  }
function isProductWithWarranty(productCode) {
    currentItemInfo = ITEMS_WITH_WARRANTY.find(function(item) {
        console.log(item);
      return item.sku === +productCode;
    });
    return currentItemInfo;
  }

  
  console.log(!isProductWithWarranty(30172028));

  var p = Promise.resolve([1,2,3]);

  p.then(function(v) {
    if (!isProductWithWarranty(30172028)) return;

    console.log(v[0]); // 1
  });
  
  (function(text) {
    console.log(text);
  })('Hi all! Welcome to our page.');
  
  var shortUrl = function (a) {
    var a = 2
  }
  console.log(shortUrl(2));

  console.log(!true);

  var productCode = '123-243-4' 
        var code = productCode.replace(/-/g, "");
        console.log(code);