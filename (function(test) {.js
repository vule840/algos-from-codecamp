(function(test) {
  'use strict';

  test = test && test.hasOwnProperty('default') ? test['default'] : test;
 
  /* variants/control/js.js */
  var control = function(_ref) {
    var cookie = _ref.cookie,
      storage = _ref.storage,
      tmpl = _ref.tmpl,
      styles = _ref.styles,
      events = _ref.events,
      Promise = _ref.Promise;
  };
  /* modules/util.js */

  function waitFor(condition, timeout) {
    var stopTime = typeof timeout === 'number' ? timeout : 7000;
    var stop = false;
    window.setTimeout(function() {
      stop = true;
    }, stopTime);
    return new Promise(function(resolve, reject) {
      (function _innerWaitFor() {
        var value = condition();

        if (stop) {
          reject();
        } else if (value) {
          resolve(value);
        } else {
          window.setTimeout(_innerWaitFor, 50);
        }
      })();
    });
  }

  function trackMetric(metricName) {
    var eVarNumber =
      arguments.length > 1 && arguments[1] !== undefined
        ? arguments[1]
        : 'eVar119';
    waitFor(function() {
      return window.s;
    }).then(function() {
      window.s[eVarNumber] = metricName;
      window.s.t();
    });
  }

  function addBodyClass(companyName, variantName) {
    document.body.classList.add(
      ''.concat(companyName, '-').concat(variantName)
    );
  }

  /* variants/variant/css.scss */
  var css =
    '.lc-dis165-warranty-block{display:none}.lc-dis165-variant .lc-dis165-warranty-block{display:block;margin-top:30px;border-top:1px solid #ececec}.lc-dis165-variant .lc-dis165-warranty-block__text{margin-top:20px;padding-left:30px;background:0/22px no-repeat url("data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjMiIGhlaWdodD0iMjMiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTIyLjI4MSA1Ljc1YzAgOS45NC02LjEwNiAxNS40ODItOS45NTIgMTcuMDg0YTIuMTU2IDIuMTU2IDAgMCAxLTEuNjU4IDBDNS44NjIgMjAuODMxLjcxOSAxNC42NjYuNzE5IDUuNzVhMi4xNTYgMi4xNTYgMCAwIDEgMS4zMjctMS45OUwxMC42Ny4xNjZhMi4xNTYgMi4xNTYgMCAwIDEgMS42NTggMGw4LjYyNSAzLjU5NGEyLjE1NiAyLjE1NiAwIDAgMSAxLjMyNyAxLjk5em0tMTEuNzEgMTEuNDA3bDguMjY1LTguMjY1YS43MTkuNzE5IDAgMCAwIDAtMS4wMTdMMTcuODIgNi44NmEuNzE5LjcxOSAwIDAgMC0xLjAxNyAwbC02Ljc0IDYuNzQtMy4xNDgtMy4xNDZhLjcxOS43MTkgMCAwIDAtMS4wMTYgMGwtMS4wMTcgMS4wMTZhLjcxOS43MTkgMCAwIDAgMCAxLjAxN2w0LjY3MiA0LjY3MWMuMjguMjgxLjczNi4yODEgMS4wMTcgMHoiIGZpbGw9IiMwMDVFODAiLz48L3N2Zz4=");font-weight:600}.lc-dis165-variant .lc-dis165-warranty-block__text:hover{cursor:pointer}';

  /* variants/variant/js.js */
  var variant = function(_ref) {
    var styles = _ref.styles;
    var _CNAME_ = 'lc-dis165';
    var _SNAME_ = 'variant';
    var LANG = document.documentElement.lang;
    var TRANSLATIONS = {
      en: {
        prefixText: '',
        text: 'warranty included',
        yearSingular: 'year',
        yearPlural: 'year',
        lifetime: 'Lifetime warranty included',
      },
      fr: {
        prefixText: 'Garantie de',
        text: 'incluse',
        yearSingular: 'an',
        yearPlural: 'ans',
        lifetime: 'Garantie à vie incluse',
      },
      de: {
        prefixText: '',
        text: 'Garantie',
        yearSingular: 'Jahr',
        yearPlural: 'Jahre',
        lifetime: 'Garantie für die Lebensdauer des Produkts',
      },
      fi: {
        prefixText: 'Sisältää',
        text: 'takuun',
        yearSingular: 'vuoden',
        yearPlural: 'vuoden',
        lifetime: 'Sisältää elinikäisen takuun',
      },
      sv: {
        prefixText: 'Sisältää',
        text: 'takuun',
        yearSingular: 'vuoden',
        yearPlural: 'vuoden',
        lifetime: 'Sisältää elinikäisen takuun',
      },
    };
    var currentItemInfo = null;
    var ITEMS_WITH_WARRANTY = [
      {
        sku: 16901348,
        warranty: 1,
      },
      {
        sku: 17605827,
        warranty: 'lifetime',
      },
      {
        sku: 17605421,
        warranty: 'lifetime',
      },
      {
        sku: 30122459,
        warranty: 3,
      },
      {
        sku: 30047575,
        warranty: 'lifetime',
      },
      {
        sku: 30122458,
        warranty: 3,
      },
      {
        sku: 17605843,
        warranty: 'lifetime',
      },
      {
        sku: 17605835,
        warranty: 'lifetime',
      },
      {
        sku: 17605447,
        warranty: 'lifetime',
      },
      {
        sku: 11030315,
        warranty: 2,
      },
      {
        sku: 30167674,
        warranty: 3,
      },
      {
        sku: 17641233,
        warranty: 2,
      },
      {
        sku: 11071704,
        warranty: 5,
      },
      {
        sku: 17608755,
        warranty: 'lifetime',
      },
      {
        sku: 17641012,
        warranty: 2,
      },
      {
        sku: 17609213,
        warranty: 'lifetime',
      },
      {
        sku: 17605408,
        warranty: 'lifetime',
      },
      {
        sku: 30047586,
        warranty: 3,
      },
      {
        sku: 30131738,
        warranty: 'lifetime',
      },
      {
        sku: 17646466,
        warranty: 1,
      },
      {
        sku: 30179252,
        warranty: 2,
      },
      {
        sku: 30108747,
        warranty: 3,
      },
      {
        sku: 30008754,
        warranty: 1,
      },
      {
        sku: 17609184,
        warranty: 3,
      },
      {
        sku: 11041944,
        warranty: 3,
      },
      {
        sku: 30037363,
        warranty: 3,
      },
      {
        sku: 30178422,
        warranty: 5,
      },
      {
        sku: 30011644,
        warranty: 3,
      },
      {
        sku: 17674286,
        warranty: 5,
      },
      {
        sku: 17600190,
        warranty: 'lifetime',
      },
      {
        sku: 30131808,
        warranty: 'lifetime',
      },
      {
        sku: 30119651,
        warranty: 'lifetime',
      },
      {
        sku: 30122508,
        warranty: 3,
      },
      {
        sku: 17671119,
        warranty: 3,
      },
      {
        sku: 30160863,
        warranty: 1,
      },
      {
        sku: 17629020,
        warranty: 1,
      },
      {
        sku: 17605821,
        warranty: 'lifetime',
      },
      {
        sku: 30122506,
        warranty: 3,
      },
      {
        sku: 16985256,
        warranty: 2,
      },
      {
        sku: 17646458,
        warranty: 1,
      },
      {
        sku: 16985121,
        warranty: 3,
      },
      {
        sku: 30092156,
        warranty: 2,
      },
      {
        sku: 17609210,
        warranty: 'lifetime',
      },
      {
        sku: 30092155,
        warranty: 2,
      },
      {
        sku: 17649395,
        warranty: 3,
      },
      {
        sku: 17640711,
        warranty: 3,
      },
      {
        sku: 17651755,
        warranty: 1,
      },
      {
        sku: 17608765,
        warranty: 'lifetime',
      },
      {
        sku: 17640709,
        warranty: 3,
      },
      {
        sku: 17609258,
        warranty: 'lifetime',
      },
      {
        sku: 30131814,
        warranty: 'lifetime',
      },
      {
        sku: 16985164,
        warranty: 3,
      },
      {
        sku: 11070977,
        warranty: 2,
      },
      {
        sku: 30118738,
        warranty: 3,
      },
      {
        sku: 30047587,
        warranty: 3,
      },
      {
        sku: 30136532,
        warranty: 1,
      },
      {
        sku: 30101671,
        warranty: 3,
      },
      {
        sku: 16971281,
        warranty: 1,
      },
      {
        sku: 30136531,
        warranty: 1,
      },
      {
        sku: 30157058,
        warranty: 3,
      },
      {
        sku: 17651804,
        warranty: 1,
      },
      {
        sku: 17610306,
        warranty: 1,
      },
      {
        sku: 30135121,
        warranty: 2,
      },
      {
        sku: 30129274,
        warranty: 3,
      },
      {
        sku: 17651805,
        warranty: 1,
      },
      {
        sku: 17651763,
        warranty: 1,
      },
      {
        sku: 30060453,
        warranty: 3,
      },
      {
        sku: 30088363,
        warranty: 2,
      },
      {
        sku: 30111506,
        warranty: 5,
      },
      {
        sku: 30020270,
        warranty: 2,
      },
      {
        sku: 30047580,
        warranty: 3,
      },
      {
        sku: 30216592,
        warranty: 1,
      },
      {
        sku: 30157057,
        warranty: 3,
      },
      {
        sku: 30035603,
        warranty: 'lifetime',
      },
      {
        sku: 30082459,
        warranty: 3,
      },
      {
        sku: 30216593,
        warranty: 1,
      },
      {
        sku: 30135119,
        warranty: 2,
      },
      {
        sku: 30089788,
        warranty: 3,
      },
      {
        sku: 30008858,
        warranty: 3,
      },
      {
        sku: 30035464,
        warranty: 3,
      },
      {
        sku: 17651951,
        warranty: 1,
      },
      {
        sku: 16984397,
        warranty: 3,
      },
      {
        sku: 30009936,
        warranty: 3,
      },
      {
        sku: 30047585,
        warranty: 3,
      },
      {
        sku: 11084986,
        warranty: 3,
      },
      {
        sku: 16984195,
        warranty: 1,
      },
      {
        sku: 17608790,
        warranty: 3,
      },
      {
        sku: 11070325,
        warranty: 1,
      },
      {
        sku: 30167895,
        warranty: 2,
      },
      {
        sku: 17651952,
        warranty: 1,
      },
      {
        sku: 30116479,
        warranty: 2,
      },
      {
        sku: 30047582,
        warranty: 3,
      },
      {
        sku: 11048971,
        warranty: 1,
      },
      {
        sku: 30108748,
        warranty: 3,
      },
      {
        sku: 17631252,
        warranty: 3,
      },
      {
        sku: 30060476,
        warranty: 3,
      },
      {
        sku: 17636291,
        warranty: 3,
      },
      {
        sku: 30092473,
        warranty: 2,
      },
      {
        sku: 30085386,
        warranty: 5,
      },
      {
        sku: 30135120,
        warranty: 2,
      },
      {
        sku: 30089789,
        warranty: 3,
      },
      {
        sku: 30176860,
        warranty: 5,
      },
      {
        sku: 11085096,
        warranty: 3,
      },
      {
        sku: 30103334,
        warranty: 1,
      },
      {
        sku: 16985841,
        warranty: 3,
      },
      {
        sku: 30074585,
        warranty: 1,
      },
      {
        sku: 30159891,
        warranty: 3,
      },
      {
        sku: 11075336,
        warranty: 5,
      },
      {
        sku: 17636290,
        warranty: 3,
      },
      {
        sku: 30020271,
        warranty: 2,
      },
      {
        sku: 11085486,
        warranty: 2,
      },
      {
        sku: 30045658,
        warranty: 2,
      },
      {
        sku: 30176862,
        warranty: 5,
      },
      {
        sku: 30045653,
        warranty: 2,
      },
      {
        sku: 11085487,
        warranty: 2,
      },
      {
        sku: 30147193,
        warranty: 2,
      },
      {
        sku: 30108360,
        warranty: 3,
      },
      {
        sku: 11085499,
        warranty: 2,
      },
      {
        sku: 17636292,
        warranty: 3,
      },
      {
        sku: 30035626,
        warranty: 3,
      },
      {
        sku: 30167896,
        warranty: 2,
      },
      {
        sku: 30158784,
        warranty: 2,
      },
      {
        sku: 11068531,
        warranty: 2,
      },
      {
        sku: 30152641,
        warranty: 3,
      },
      {
        sku: 30185783,
        warranty: 3,
      },
      {
        sku: 11085443,
        warranty: 2,
      },
      {
        sku: 30075605,
        warranty: 1,
      },
      {
        sku: 11070328,
        warranty: 1,
      },
      {
        sku: 30200926,
        warranty: 2,
      },
      {
        sku: 30164161,
        warranty: 3,
      },
      {
        sku: 30152655,
        warranty: 3,
      },
      {
        sku: 30178519,
        warranty: 1,
      },
      {
        sku: 30177301,
        warranty: 2,
      },
      {
        sku: 30200923,
        warranty: 2,
      },
      {
        sku: 30145097,
        warranty: 5,
      },
      {
        sku: 17661690,
        warranty: 1,
      },
      {
        sku: 30159890,
        warranty: 3,
      },
      {
        sku: 30108361,
        warranty: 3,
      },
      {
        sku: 11085448,
        warranty: 2,
      },
      {
        sku: 30186127,
        warranty: 3,
      },
      {
        sku: 11068529,
        warranty: 2,
      },
      {
        sku: 30148542,
        warranty: 1,
      },
      {
        sku: 30162750,
        warranty: 1,
      },
      {
        sku: 30104375,
        warranty: 3,
      },
      {
        sku: 30100570,
        warranty: 3,
      },
      {
        sku: 11085506,
        warranty: 2,
      },
      {
        sku: 30159888,
        warranty: 3,
      },
      {
        sku: 11085455,
        warranty: 2,
      },
      {
        sku: 30145107,
        warranty: 5,
      },
      {
        sku: 17636320,
        warranty: 5,
      },
      {
        sku: 11070326,
        warranty: 1,
      },
      {
        sku: 30172028,
        warranty: 3,
      },
    ];
    waitFor(function() {
      return (
        typeof window.jQuery === 'function' &&
        $('#product_code').length &&
        $('#product_code') &&
        $('#product_code').val()
      );
    })
      .then(function(productCode) {
        if (!isProductWithWarranty(productCode)) return;
        startCampaign()
        /* variants/load.js */
      })
      .catch(function(error) {
        // eslint-disable-next-line no-console
        console.warn(''.concat(_CNAME_, '-').concat(_SNAME_, ' Error:'), error);
      });

    function startCampaign() {
      addBodyClass(_CNAME_, _SNAME_);
      styles.add(css);
      renderLcWarrantyBlock();
      onLcWarrantyClick();
    }

    function renderLcWarrantyBlock() {
      var nodeToAppend = $('.product-right-info__bottom');
      var warrantyVal = currentItemInfo.warranty;
      var warrantyDurationName = null;

      if (typeof warrantyVal === 'string') {
        warrantyDurationName = TRANSLATIONS[LANG].lifetime;
        nodeToAppend.after(
          '<div class='
            .concat(_CNAME_, '-warranty-block><p class=')
            .concat(_CNAME_, '-warranty-block__text> ')
            .concat(warrantyDurationName, '</p></div>')
        );
      } else {
        warrantyDurationName =
          warrantyVal > 1
            ? TRANSLATIONS[LANG].yearPlural
            : TRANSLATIONS[LANG].yearSingular;
        if (warrantyVal === 1) TRANSLATIONS.fr.prefixText = 'Garantie';
        nodeToAppend.after(
          '<div class='
            .concat(_CNAME_, '-warranty-block><p class=')
            .concat(_CNAME_, '-warranty-block__text>')
            .concat(TRANSLATIONS[LANG].prefixText, ' ')
            .concat(warrantyVal, ' ')
            .concat(warrantyDurationName, ' ')
            .concat(TRANSLATIONS[LANG].text, '</p></div>')
        );
      }
    }

    function onLcWarrantyClick() {
      $('.'.concat(_CNAME_, '-warranty-block')).click(function() {
        trackMetric(''.concat(_CNAME_, '-').concat(_SNAME_, '-warranty-click'));
        $('.mod-detail-tabs .specification-holder')[0].scrollIntoView(true);
      });
    }

    function isProductWithWarranty(productCode) {
      currentItemInfo = ITEMS_WITH_WARRANTY.find(function(item) {
        return item.sku === +productCode;
      });
      return currentItemInfo;
    }
    console.log(isProductWithWarranty(productCode));
  };

    var campaignConfig = {
    name: 'MVT_0391|dis165-brand-warranty-messaging',
    // Standard campaign's name must have client agreed prefix followed by | sign
    // name: 'a-site-metrics' // Any name without prefix is treated as service campaign and won't be tracked by Adobe
    variants: {
      control: {
        weight: 50,
        code: control,
      },
      variant: {
        weight: 50,
        code: variant,
      },
    },
  };
  test.runWithModules(function(_ref) {
    var cookie = _ref.cookie,
      storage = _ref.storage;
    storage.set('example', 3, 3);
    cookie.set('example', 3, 3); // loads leanOptimiser campaign - to be called after all targeting rules are checked and truthy

    test.load(campaignConfig);
  });
})(window.lc.test);
//# sourceURL=url://lc.test/c--dis165-brand-warranty-messaging.js
