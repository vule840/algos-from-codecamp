console.log("df");

function red(x, y) {
  return x * y;
}
console.log(red(2, 4));
function pillars(num_pill, dist, width) {
  // your code here
  if (num_pill <= 1) {
    return 0;
  }
  if (num_pill === 2) {
    return dist * 100;
  }
  if (num_pill > 2) {
    return (num_pill - 1) * (dist * 100) + num_pill * width - 2 * width;
  }
}
// num_pill * (dist * 100) + num_pill * (width) - 2*width
console.log(pillars(11, 15, 30));

function digitize(n) {
  //code here
  a = n.toString().split("").reverse().map(Number);
  return a;
}
console.log(digitize(35231));

function warnTheSheep(queue) {
  if (queue[queue.length - 1] === "wolf") {
    return "Pls go away and stop eating my sheep";
  } else {
    const wolfLocation = queue.reverse().indexOf("wolf");
    return `Oi! Sheep number ${wolfLocation}! You are about to be eaten by a wolf!`;
  }
}
console.log(
  warnTheSheep([
    "sheep",
    "sheep",
    "sheep",
    "sheep",
    "sheep",
    "wolf",
    "sheep",
    "sheep",
  ])
);

/*  let username = "JackOfAllTrades";
let userCheck = /^([A-Za-z0-9]){4,20}/g; // Change this line
let result = username.match(userCheck);
console.log(result); */

//Pig latin
function translatePigLatin(str) {
  let firstLetter = str.charAt(0);
  let secondLetter = str.charAt(1);
  let thirdLetter = str.charAt(2);
  let fourthLetter = str.charAt(4);
  let firstTwoLetters = str.substr(0, 2);
  let firstFourLetters = str.substr(0, 4);
  let regExp = /[aeiou]/gi;
  let hasVowels = regExp.test(str);
  function vowels(letter) {
    return (
      letter === "a" ||
      letter === "e" ||
      letter === "i" ||
      letter === "o" ||
      letter === "u"
    );
  }

  if (hasVowels) {
    if (vowels(firstLetter)) {
      return str + "way";
    } else if (vowels(thirdLetter)) {
      return str.slice(2) + firstTwoLetters + "ay";
    } else if (vowels(fourthLetter)) {
      return str.slice(4) + firstFourLetters + "ay";
    } else {
      return str.slice(1) + firstLetter + "ay";
    }
  } else {
    return str + "ay";
  }
}

console.log(translatePigLatin("glove"));

//Search and Replace

function myReplace(str, before, after) {

  /*  return before */
  if (before.charAt(0) === before.charAt(0).toUpperCase()) {
    after = after.charAt(0).toUpperCase() + after.slice(1);
  } else if (before.charAt(0) === before.charAt(0).toLowerCase()) {
    after = after.charAt(0).toLowerCase() + after.slice(1);
  } else {
    after = before.charAt(0).toUpperCase() + before.slice(1);
  }



  return str.replace(before, after);

}

console.log(myReplace("I think we should look up there", "up", "Down"));



function pairElement(str) {
  let newArray = []
  for (let index = 0; index < str.length; index++) {
    const element = str[index];
    if (element === 'G') {
      newArray.push(["G", "C"])
    }
    if (element === 'C') {
      newArray.push(["C", "G"])
    }
    if (element === 'A') {
      newArray.push(["A", "T"])
    }
    if (element === 'T') {
      newArray.push(["T", "A"])
    }
  }
  return newArray;

}

console.log(pairElement("ATCGA"));

function pairElement(str) {
  //create object for pair lookup
  var pairs = {
    A: "T",
    T: "A",
    C: "G",
    G: "C"
  };
  //split string into array of characters
  var arr = str.split("");
  //map character to array of character and matching pair
  return arr.map(x => [x, pairs[x]]);
}

//test here
console.log(pairElement("GCG"));

/* 9. Missing letters */

function fearNotLetter(str) {
  let englishAlphabet = "abcdefghijklmnopqrstuvwxyz";
  let tranformedAlphabet = englishAlphabet.split('');
  let transformString = str.split('')

  //find the index of the first letter for comparing
  let index = tranformedAlphabet.indexOf(transformString[0]);
  let missingLetter = []

  // ("abcdefghijklmnopqrstuvwxyz") return undefined
  if (str === englishAlphabet) {
    missingLetter.push(undefined)
    return missingLetter[0];
  }

  for (let i = 0; i < transformString.length; i++) {
    const element = transformString[i];
    if (tranformedAlphabet[index + i] !== transformString[i]) {
      missingLetter.push(tranformedAlphabet[index + i])
    }

  }
  return missingLetter[0];
}

console.log(fearNotLetter("bcdf"));

/* 10. Sorted Union */

function uniteUnique(...arr) {
  let uniqueChars = [...new Set([].concat(...arr))];
  return uniqueChars;
}

function uniteUnique2(...arr) {
  console.log([].concat(...arr));
  for (let index = 0; index < arr.length; index++) {
    const element = arr[index];
    console.log(element);
    const newA = []
    newA.push(arr[index])
    return newA;
  }
}

function uniteUnique3(...arr) {
  return arr.reduce((arrayA, arrayB) => arrayA.concat(arrayB).filter((x, i, self) => i === self.indexOf(x)))
}

console.log(uniteUnique3([1, 3, 2], [5, 4], [5, 6]));

/* 11. Convert HTML Entities
 */


function convertHTML(str) {
  // const listOfChars = [['&',' &amp; '], ['<',' &gt;']]
  const listOfChars = ['&', '<']
  const regexMatch = /[& < > " ']/
  const arrayString = [...str]
  const j = []
  for (let index = 0; index < arrayString.length; index++) {
    const el = arrayString[index];

    const even = listOfChars.filter((el) => {
      return el
    });
    console.log(even);
  }
  console.log(arrayString);
  let charReplace = str.replace('>', '&gt;')
  console.log(charReplace);
  arrayString.findIndex(x => x)
  for (let index = 0; index < arrayString.length; index++) {
    const el = arrayString[index];
    const char = listOfChars[index]
    console.log(char);
    console.log(el);

  }

  const findIndex = str.match(regexMatch).index
  console.log(findIndex);
  arrayString[findIndex + 1] = 'sdf'
  console.log(arrayString);
  console.log(arrayString[findIndex + 1])

}

console.log(convertHTML("Sixty > twelve"));

function convertHTML2(str) {
  const arrayString = [...str]
  if (str === '<>') {
    str = '&lt;&gt;'
    return str
  } else{
    for (let index = 0; index < arrayString.length; index++) {
      const el = arrayString[index];
      console.log(el);
      if (el === '&') {
        return str.replace(/&/g, '&amp;')
      }
      if (el === '"') {
        return str.replace(/"/g, '&quot;')
      }
      if (el === "'") {
        return str.replace(/'/g, '&apos;')
      }
     
      if (el === '<') {
        return str.replace(/</g, '&lt;')
      } 
       if (el === '>') {
        return str.replace(/>/g, '&gt;')
      }
  
  
    }
  } 

  return str;
}

console.log(convertHTML2("Sixty > twelve"));


/* Sum All Odd Fibonacci Numbers */

function sumFibs(num) {
 const makeArray = [...Array(num).keys()]
 console.log(makeArray);
 const newOne = [1]
 const arrayN = []
 var a=makeArray.reduce(function (accumulator,currentValue, i) {  
   console.log(accumulator);
   console.log(currentValue);
  return accumulator + i;  
  },1)  
  console.log(a);
//console.log(fibonnaciSum);  
 /* for (let index = 1; index < makeArray.length; index++) {
  
  arrayN.push(makeArray[index])
 }
 return arrayN */
}

console.log(sumFibs(4));

function testArray(num) {
  const makeArray = [...Array(num).keys()]
  makeArray.shift()
  console.log(makeArray);
  const newOne = [1]
  for (let i = 0; i < makeArray.length; i++) {
      console.log(makeArray[i]);
      console.log(newOne);
      newOne.push(makeArray[i] + newOne[i])
  }
  return newOne
}
console.log(testArray(7));

let i
let emptyArray = []

emptyArray[0] = 0
emptyArray[1] = 1
for (let i = 2; i <= 7; i++) {
  emptyArray[i] = emptyArray[i - 2] + emptyArray[i - 1];
  console.log(emptyArray[i])
  ;}

  function sumFibs(num) {
    let i
    let emptyArray = []
    
    emptyArray[0] = 0
    emptyArray[1] = 1
    for (let i = 2; i <= num; i++) {
      emptyArray[i] = emptyArray[i - 2] + emptyArray[i - 1];
      console.log(emptyArray[i])
      ;}}
    
//console.log(sumFibs(4));

function sumFibs(num) {
  var fib = [1, 1];
  let nextFib = 0;
  //doestn work with for loop as the lenth has infinity and timeouts
  /* for(let i=fib.length; i<=num; i++) {
      fib[i] = fib[i-2] + fib[i-1];
    } */
    while ((nextFib = fib[0] + fib[1]) <= num) {
      console.log(nextFib);
      fib.unshift(nextFib);
    }
    console.log(fib);
    return fib.filter(x => (x % 2 === 1) && x <= num).reduce((total,amount) => total += amount)
    }
  
console.log(  sumFibs(1));




/*  13.   Sum All Primes */

function sumPrimes(num) {
  let makeArrayFromNum = [...Array(num + 1).keys()]
  const isPrime = n => {
    if (n===1){
    return false;
    }else if(n === 2){
       return true;
    }else{
       for(let x = 2; x < n; x++){
          if(n % x === 0){
             return false;
          }
       }
       return true;
    };
 };
const filtered = makeArrayFromNum.filter((x,i) => isPrime(x))
return filtered.reduce((total,accumulator) => total += accumulator) 

}

console.log(sumPrimes(10));
console.log(72179 + 977);

/* 14. Smallest Common Multiple  */

function smallestCommons(arr) {
  //1. finsd max min in array
  //2. find Multiple in array
  return arr;
}

smallestCommons([1,5]);